#include <gtest/gtest.h>
#include "extratree.h"

int n = 30000;

TEST(ExtratreeTest, split) {  
  srand48(time(NULL));
  std::vector<ExtraTreePattern> v;  
  for (int i=0; i<150; ++i) {
    ExtraTreePattern pattern;
    pattern.output = 0.0;
    pattern.input = new double[1];
    pattern.input[0] = drand48();
    v.push_back(pattern);
  }  
  ExtraTreeNode node;
  node.value = drand48();
  node.test = 0;
  int n = ExtraTree::split(v, &node, 0, v.size());
  for (int i=0; i<n; i++) ASSERT_LE(v[i].input[0], node.value);
  for (int i=n; i<v.size(); i++) ASSERT_GE(v[i].input[0], node.value);
}

TEST(ExtratreeTest, random) { 
  unsigned int seed = time(NULL);
  double min, max, a, b, r;
  for (int i=0; i<1e7; ++i) {
    a = (drand48()-0.5)*2000;
    b = (drand48()-0.5)*2000;
    if (a<b) {
      min=a; 
      max=b;
    } else if (b>a) {
      min=b; 
      max=a;
    } else continue;
    r = ExtraTree::random(min, max, &seed);
    ASSERT_GT(r, min);
    ASSERT_LT(r, max);
  }
}

TEST(ExtratreeTest, is_constant) {
  
  std::vector<ExtraTreePattern> v;
  for (int i=1; i<1000; ++i) {
    ExtraTreePattern pattern;
    pattern.output = (double)i/(double)i;
    v.push_back(pattern);
  }
  ASSERT_TRUE(ExtraTree::is_constant(v));  
  ASSERT_TRUE(ExtraTree::is_constant(v, 0, 999));  
  ASSERT_TRUE(ExtraTree::is_constant(v, 100, 800));  
  ASSERT_TRUE(ExtraTree::is_constant(v, 800, 801));  
  for (int i=0; i<10; ++i) {
    for (int j=1; j<10; ++j) {
      ExtraTreePattern pattern;
      pattern.output = (double)i/(double)j;
      v.push_back(pattern);
    }
  }
  ASSERT_TRUE(ExtraTree::is_constant(v, 0, 999));  
  ASSERT_TRUE(ExtraTree::is_constant(v, 100, 800));  
  ASSERT_TRUE(ExtraTree::is_constant(v, 800, 801));
  ASSERT_FALSE(ExtraTree::is_constant(v));
  ASSERT_FALSE(ExtraTree::is_constant(v, 0, 1088));  
  ASSERT_FALSE(ExtraTree::is_constant(v, 999, 1050));  
}

TEST(ExtratreeTest, empty) {
  ExtraTree tree(2);
  double x[2] = {0.0, 0.0};
  double y[1] = {drand48()};
  ASSERT_NEAR(0.0, tree.predict(x), 1e-12);
  ASSERT_NEAR(0.0, tree.predict(y), 1e-12);
}

TEST(ExtratreeTest, constant) {
  ExtraTree tree(1);
  for (int i=0; i<n; ++i) {
    double x[1] = {2.0*i/n - 1.0};
    tree.add_pattern(x, 0.4);
  }
  tree.train(10, 1, 20);
  ASSERT_EQ(10, tree.num_trees());
  for (int i=0; i<n; ++i) {
    double x[1] = {2.0*i/n - 1.0};
    ASSERT_NEAR(0.4, tree.predict(x), 1e-12);
  } 
}

TEST(ExtratreeTest, identity) {
  ExtraTree tree(1);
  for (int i=0; i<n; ++i) {
    double x[1] = {2.0*i/n - 1.0};
    tree.add_pattern(x, x[0]);    
  }
  tree.train(50, 2, 5);
  ASSERT_EQ(50, tree.num_trees());
  for (int i=0; i<n; ++i) {
    double x[1] = {2.0*i/n - 1.0};
    ASSERT_NEAR(x[0], tree.predict(x), 1e-2);
  } 
}

TEST(ExtratreeTest, polynomial) {
  ExtraTree tree(2);
  int num_trees = 80;
  for (int i=0; i<n; ++i) {    
    double x[2] = {drand48()-0.5, drand48()-0.5};
    tree.add_pattern(x, x[0]+2*x[0]*x[1]+4*x[1]);    
  }
  tree.train(num_trees, 2, 5);
  ASSERT_EQ(num_trees, tree.num_trees());
  for (int i=0; i<n; ++i) {
    double x[2] = {drand48()-0.5, drand48()-0.5};
    ASSERT_NEAR(x[0]+2*x[0]*x[1]+4*x[1], tree.predict(x), 0.1);
  } 
}

TEST(ExtratreeTest, readwrite) {
  ExtraTree tree(2);
  for (int i=0; i<n; ++i) {    
    double x[2] = {2.0*drand48()-1.0, 2.0*drand48()-1.0};
    tree.add_pattern(x, x[0]+2*x[0]*x[1]+4*x[1]);    
  }
  tree.train(50, 1, 15);
  tree.write("testfile");
  ExtraTree treefromfile("testfile");
  ASSERT_EQ(50, tree.num_trees());
  ASSERT_EQ(50, treefromfile.num_trees());
  for (int i=0; i<n; ++i) {
    double x[2] = {2.0*drand48()-1.0, 2.0*drand48()-1.0};
    ASSERT_NEAR(tree.predict(x), treefromfile.predict(x), 1e-12);
  }
}